#include <iostream>
#include <cstring>
#include <chrono>
#include <algorithm>

void testMemMove() {
	std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
	start = std::chrono::high_resolution_clock::now();

	int arr[100000];

	for(int i = 50; i < 2000; i++) {
		arr[i] = i;
	}
	
	int k = 100000000;
	while(k-- != 0)
		std::memmove( (void*)&arr[100000 - 2000], (const void*)&arr[50], 2000);


	finish = std::chrono::high_resolution_clock::now();
	std::cout << "Memmove "
	<< std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
	<< std::endl;
}

int main() {

	testMemMove();

	return 0;
}