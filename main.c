#include <stdio.h>
#include <stdlib.h>
#include "src/btree.h"

int main(int argc, char *argv[]) {
    BTree* btree = btreeInit(4, 1, malloc, realloc, free, free, &btreeLexicalComparator);
    if(btree == NULL)
        printf("Error creating btree!\n");
    else {
        printf("Btree successfully created!\n");
        
        
        btreeInsert(btree, (void*)"a", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"b", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"c", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"d", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"e", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"f", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"g", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"h", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"i", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"j", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"k", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"l", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"m", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"n", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"o", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"p", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"q", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"r", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"s", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"t", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"u", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"v", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"w", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"x", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"y", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"z", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"e", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"f", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"g", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"h", (void*)"efwefefwefwe");
        btreePrintNode(btree->root);
        btreeInsert(btree, (void*)"A", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"B", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"C", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"D", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"E", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"F", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"G", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"H", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"I", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"J", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"K", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"L", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"M", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"N", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"O", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"P", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"Q", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"R", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"S", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"T", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"U", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"V", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"W", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"X", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"Y", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"Z", (void*)"222aafefefef");
        btreeInsert(btree, (void*)"E", (void*)"efwefefwefwe");
        btreeInsert(btree, (void*)"F", (void*)"122aafefefef");
        btreeInsert(btree, (void*)"G", (void*)"522aafefefef");
        btreeInsert(btree, (void*)"H", (void*)"efwefefwefwe");
        /*for(int i = 1; i < 128; i++){
            int k = i;
            btreeInsert(btree, (void*)&k, (void*)"efwefefwefwe");
            //printf("inserting %d %c \n", i, i);
        }*/
        
        printf("Size: %ul %ul \n", sizeof(BTree), sizeof(BTreeNode));
        printf("NODE COUNT %u, %u\n", btree->nodeCount, btree->itemCount);
        //btreePrintNode(btree->root);
    }
    return 0;
}
