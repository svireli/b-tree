#include <iostream>
#include <string>
#include <forward_list>
#include <chrono>
#include <algorithm>

inline void log(std::string text) { std::cout << text << std::endl; }

template <typename T>
class BTreeNode {
  unsigned int childCount = 0;
  unsigned int keyCount = 0;
  unsigned int t = 0;
  std::forward_list<std::pair<T, BTreeNode<T>*> > keyChildPairs;
  BTreeNode<T>* firstChild;

 public:
  BTreeNode(unsigned int _t) : t(_t) {}

  ~BTreeNode() {
    if(!this->isLeaf()) {
		auto it = this->keyChildPairs.begin();
		while(it != this->keyChildPairs.end()) {
			delete (*it).second;
			it++;
		}
	}
  }

  inline bool isLeaf() { return this->childCount == 0; }

  inline bool isEmpty() { return this->keyCount == 0 && this->isLeaf(); }

  inline bool isFull() { return this->keyCount == 2 * t - 1; }

  inline void insertFirstKey(T key) {
    this->keyChildPairs.push_front(std::pair<T, BTreeNode<T>* >(key, nullptr));
    this->keyCount = 1;
  }

  inline typename std::forward_list<std::pair<T, BTreeNode<T>* > >::iterator insertFirstChild(
      BTreeNode<T>* node) {
    this->firstChild = node;
    this->childCount = 1;
    return this->keyChildPairs.before_begin();
  }

  void splitChild(typename std::forward_list<std::pair<T, BTreeNode<T>* > >::iterator leftNodeIterator) {
    BTreeNode<T>* leftNode = leftNodeIterator == this->keyChildPairs.before_begin() ? firstChild : (*leftNodeIterator).second;
    BTreeNode<T>* rightNode = new BTreeNode<T>(this->t);

    auto it = leftNode->keyChildPairs.begin();
    unsigned int i = this->t;
    while(--i != 1) ++it;
    rightNode->firstChild = (*std::next(it)).second;
    rightNode->keyChildPairs.splice_after(rightNode->keyChildPairs.before_begin(), leftNode->keyChildPairs,
								std::next(it), leftNode->keyChildPairs.end());
    rightNode->keyCount = this->t - 1;
    leftNode->keyCount = this->t - 1;

    if(!leftNode->isLeaf()){
        rightNode->childCount = this->t;
        leftNode->childCount = this->t;
    }

    if(leftNodeIterator == this->keyChildPairs.before_begin()) {
        this->keyChildPairs.push_front(std::pair<T, BTreeNode<T>*>((*std::next(it)).first, rightNode));
    } else {
        this->keyChildPairs.insert_after(leftNodeIterator, std::pair<T, BTreeNode<T>*>((*std::next(it)).first, rightNode));
    }
	leftNode->keyChildPairs.erase_after(it);
    this->keyCount++;
    this->childCount++;	
	return;
  }

  void insertKey(T key) {
    if (this->isLeaf()) {
      if (key == this->keyChildPairs.front().first) return;
      if (key < this->keyChildPairs.front().first) {
        this->keyChildPairs.push_front(std::pair<T, BTreeNode<T>*>(key, nullptr));
      } else {
        auto it = this->keyChildPairs.begin();
        while (std::next(it) != this->keyChildPairs.end() && key > (*std::next(it)).first) ++it;
        auto nit = std::next(it);
        if (nit != this->keyChildPairs.end() && key == (*nit).first) return;
        this->keyChildPairs.insert_after(it, std::pair<T, BTreeNode<T>*>(key, nullptr));
      }
      this->keyCount++;
    } else {
      auto it = this->keyChildPairs.before_begin();
      BTreeNode<T> *childNode;
      if (key < this->keyChildPairs.front().first) {
            childNode = this->firstChild;
      }
      else {
            it = this->keyChildPairs.begin();
            while (std::next(it) != this->keyChildPairs.end() && key > (*std::next(it)).first) it++;
            childNode = (*it).second;
      }

      if (childNode->isFull()) {
        this->splitChild(it);

        if(it == this->keyChildPairs.before_begin()) {
            if(key < this->keyChildPairs.front().first) {
                this->firstChild->insertKey(key);
            } else {
                this->keyChildPairs.front().second->insertKey(key);
            }
        } else {			
            if((key < (*std::next(it)).first)){
                childNode->insertKey(key);
			}
            else{
                (*std::next(it)).second->insertKey(key);
			}
         }
      } else
        childNode->insertKey(key);
    }
  }

  T findKey(T key) {
	if(this->isLeaf()) {
		auto it = this->keyChildPairs.begin();
		while(it != this->keyChildPairs.end() && (*it).first != key)
			++it;
		if(it == this->keyChildPairs.end())
			return T();
		else
			return (*it).first;
	} else {
		auto it = this->keyChildPairs.begin();
		if(key == (*it).first)
            return (*it).first;
		if(key < (*it).first)
            return this->firstChild->findKey(key);
		while (std::next(it) != this->keyChildPairs.end() && key > (*std::next(it)).first) ++it;
        if(std::next(it) != this->keyChildPairs.end() && key == (*std::next(it)).first)
            return (*std::next(it)).first;

		return (*it).second->findKey(key);
	}
  }

  void print() {
    log("=== Printing Node ===");
    std::cout << "Node Key Count: " << this->keyCount
              << " Child Count: " << this->childCount << std::endl;
	if(this->keyCount > 0)
		for (auto keyChildPair : this->keyChildPairs) std::cout << " | " << keyChildPair.first << std::endl;
	
	if(this->childCount > 0) {
		this->firstChild->print();
		for (auto keyChildPair : this->keyChildPairs) keyChildPair.second->print();
	}
    log("=== Printing Node End ===");
  }
};

template <typename T>
class BTree {
  unsigned int t;
  unsigned int keyCount = 0;
  BTreeNode<T>* root;

 public:
  BTree(unsigned int _t) : t(_t) { root = nullptr; }

  ~BTree() {
    delete root;
  }

  void insert(T key) {
    if (this->root == nullptr) {
      this->root = new BTreeNode<T>(this->t);
      this->root->insertFirstKey(key);
    } else {
      if (this->root->isFull()) {
        BTreeNode<T>* nodeToSplit = this->root;
        this->root = new BTreeNode<T>(this->t);
        this->root->splitChild(this->root->insertFirstChild(nodeToSplit));
      }
      this->root->insertKey(key);
    }
  }

  T find(T key) {
	return this->root->findKey(key);
  }

  void print() {
    log("==== Printing Tree ====");
    std::cout << "Tree Key Count: " << this->keyCount << std::endl;
    if (this->root) this->root->print();
    log("==== Printing Tree End ====");
  }
};

class KeyValue {
	int key;
	int value;
public:
	KeyValue(int k, int v): key(k), value(v) { }
	~KeyValue() {  }
	int getKey() { return this->key; }
	int getValue() { return this->value; }
	inline bool operator > (KeyValue& rhs) {
		return this->key > rhs.getKey();
	}

	inline bool operator < (KeyValue& rhs) {
		return this->key < rhs.getKey();
	}

	inline bool operator != (KeyValue& rhs) {
		return this->key != rhs.getKey();
	}

	inline bool operator == (KeyValue& rhs) {
		return this->key == rhs.getKey();
	}

	friend std::ostream& operator<<(std::ostream& os, const KeyValue &kv) {
		os << kv.key;
		return os;
	}
};

#define TEN_MILLION 10000000
int* integers;

void generateAndFillIntegers()
{
	integers = new int[TEN_MILLION];
	for (size_t i = 0; i < TEN_MILLION; i++) {
		integers[i] = i - (TEN_MILLION / 2);
	}
	std::random_shuffle(integers, integers + TEN_MILLION);
}

int main() {

  /*BTree<int> tr(3);
  generateAndFillIntegers();
  log("testing insert");
  for (int i = 0; i <= TEN_MILLION; ++i) {
	//std::cin.ignore();
    std::cout << i << "  " << integers[i] << std::endl;
	tr.insert(integers[i]);
  }
  log("testing search");
  for (int i = 0; i <= TEN_MILLION; ++i) {
	std::cout << tr.find(integers[i]) << std::endl;
  }*/
  generateAndFillIntegers();
  log("Random Shuffled List Insert!");
  /*for(int t = 2; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i <= TEN_MILLION; ++i) {
        tree.insert(integers[i]);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }*/

  log("Sorted List Insert!");
  for(int t = 2; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i <= TEN_MILLION; ++i) {
        tree.insert(i);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }

  log("Reverse sorted list insert!");
  for(int t = 2; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = TEN_MILLION; i >0; --i) {
        tree.insert(i);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }
  return 0;
}
