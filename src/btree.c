#include "btree.h"
#include <string.h>

BTree* btreeInit(size_t t, size_t ks,
                 MemoryAllocatorFunctionType ma,
                 MemoryReallocatorFunctionType mra,
                 MemoryCollectorFunctionType mc,
                 MemoryCollectorFunctionType imc,
                 KeyComparatorFunctionType kc) {
    if(t < 2)
        return NULL;
    if(ks == 0)
        return NULL;
    BTree *tree = ma(sizeof(BTree));
    tree->t = t;
    tree->maxOrder = 2*t - 1;
    tree->minOrder = t - 1;
    tree->splitIndex = (2*t - 1) / 2;
    tree->keySize = ks;
    tree->root = NULL;
    tree->itemCount = 0;
    tree->nodeCount = 0;
    tree->memAllocator = ma;
    tree->memReallocator = mra;
    tree->memCollector = mc;
    tree->itemMemCollector = imc;
    tree->keyComparator = kc;
    return tree;
}

uint32_t btreeInsert(BTree* tree, void* key, void* value) {
    if(BTREE_IS_EMPTY(tree)) {
        tree->root = btreeCreateEmptyNode(tree->memAllocator, tree->maxOrder);
        //btreeInsertInEmptyNode(tree->memAllocator, tree->root, key, value);
        tree->root->startKeyPair = (BTreeNodeKeyValuePair*)tree->memAllocator(sizeof(BTreeNodeKeyValuePair));
        tree->root->startKeyPair->key = key;
        tree->root->startKeyPair->value = value;
        tree->root->keyCount = 1;
        tree->root->startKeyPair->next = NULL;
        tree->itemCount = 1;
        tree->nodeCount = 1;
    } else {
        BTreeNode *parent = NULL;
        BTreeNode *node = tree->root;
        //printf("inserting: %s \n", (char*)key);
        while(1) {
            if(BTREE_NODE_IS_FULL(tree, node)) {
                node = btreeSplitNode(tree, parent, node);
            }
            if(BTREE_NODE_IS_LEAF(node)) {
                //printf("is leaf\n");
                //printf("inserting: %s \n", (char*)key);
                int32_t comparison = tree->keyComparator(key, node->startKeyPair->key, tree->keySize);
                if(comparison == 0) {
                    node->startKeyPair->value = value;
                    return BTREE_SUCCESS;
                }

                if(comparison > 0) {
                    BTreeNodeKeyValuePair *kvIterator = node->startKeyPair;
                    while(kvIterator->next != NULL && tree->keyComparator(key, kvIterator->next->key, tree->keySize) >= 0) {
                        //printf("%s %s %d \n", (char*)key, (char*)kvIterator->next->key, tree->keyComparator(key, kvIterator->next->key, tree->keySize));
                        kvIterator = kvIterator->next;
                    }
                    //printf("next to %s\n", (char*)kvIterator->key);
                    if(tree->keyComparator(key, kvIterator->key, tree->keySize) == 0) {
                        kvIterator->value = value;
                        return BTREE_SUCCESS;
                    } else {
                        kvIterator->next = btreeCreateKeyValuePair(tree->memAllocator, key, value, kvIterator->next);
                    }
                } else {
                    node->startKeyPair = btreeCreateKeyValuePair(tree->memAllocator, key, value, node->startKeyPair);
                }

                node->keyCount++;
                tree->itemCount++;

                return 1;
            } else {
                BTreeNodeKeyValuePair *keyPair = node->startKeyPair;
                int i = 0;
                while(keyPair != NULL && tree->keyComparator(key, keyPair->key, tree->keySize) >= 0) {
                    keyPair = keyPair->next;
                    i++;
                }
                BTreeNode *nodeIterator = node->startChild;
                while(--i >= 0) {
                    nodeIterator = nodeIterator->next;
                }
                parent = node;
                node = nodeIterator;
                
            }
        }
    }

    return BTREE_ERROR;
}

BTreeNode* btreeCreateEmptyNode(MemoryAllocatorFunctionType ma, size_t keyCount) {
    BTreeNode *node = ma(sizeof(BTreeNode));
    node->startKeyPair = NULL;
    node->keyCount = 0;
    node->next = NULL;
    node->startChild = NULL;
    node->childCount = 0;
    return node;
}

BTreeNodeKeyValuePair* btreeCreateKeyValuePair(MemoryAllocatorFunctionType ma, void* key, void* value, void* next) {
    BTreeNodeKeyValuePair *kv = ma(sizeof(BTreeNodeKeyValuePair));
    kv->key = key;
    kv->value = value;
    kv->next = next;
    return kv;
}

void btreeInsertInEmptyNode(MemoryAllocatorFunctionType ma, BTreeNode* node, void* key, void* value) {
    node->startKeyPair = (BTreeNodeKeyValuePair*)ma(sizeof(BTreeNodeKeyValuePair));
    node->startKeyPair->key = key;
    node->startKeyPair->value = value;
    node->keyCount = 1;
    node->startKeyPair->next = NULL;
}

BTreeNode* btreeSplitNode(BTree* tree, BTreeNode* parent, BTreeNode* node) {
    if(BTREE_NODE_IS_ROOT(tree, node) && parent == NULL) {
        //printf("tree is root\n");
        tree->root = btreeCreateEmptyNode(tree->memAllocator, tree->maxOrder);
        parent = tree->root;
    }
    //printf("splitting in index %d\n", tree->splitIndex);

    BTreeNode *rightNode = btreeCreateEmptyNode(tree->memAllocator, tree->maxOrder);

    //i is required for index iteration
    int i = 0;
    //prepare pointers for list splitting.
    BTreeNodeKeyValuePair *spliterKvPair = node->startKeyPair;
    BTreeNodeKeyValuePair *firstListEnd = NULL;
    BTreeNodeKeyValuePair *secondListStart = NULL;
    //go to spliter element in the middle
    while(i++ < tree->splitIndex){
        firstListEnd = spliterKvPair;
        spliterKvPair = spliterKvPair->next;
    }

    //remove links, so we have 2 separated linked list and middle kv pair.
    secondListStart = spliterKvPair->next;
    spliterKvPair->next = NULL;
    firstListEnd->next = NULL;
    //printf("%s %s %s \n", (char*)firstListEnd->key, (char*)spliterKvPair->key, (char*)secondListStart->key);

    rightNode->startKeyPair = secondListStart;
    rightNode->keyCount = tree->minOrder;
    node->keyCount = tree->minOrder;

    if(!BTREE_NODE_IS_LEAF(node)) {
        node->childCount = tree->t;
        rightNode->childCount = tree->t;
        i = tree->t - 1;
        BTreeNode* nodeIterator = rightNode->startChild;
        while(i-- >= 0)
            nodeIterator = nodeIterator->next;
        rightNode->startChild = nodeIterator->next;
        nodeIterator->next = NULL;
    }


    if(BTREE_NODE_IS_EMPTY(parent)) {
        //printf("parent is empty\n");
        parent->startKeyPair = spliterKvPair;
        parent->keyCount = 1;
        parent->childCount = 2;
        parent->startChild = node;
        node->next = rightNode;
    } else {
        i = 0;
        //btreePrintNode(parent);
        //btreePrintNode(rightNode);
        //printf("printing some shit %s\n", spliterKvPair->key);
        //printf("index is --- %d\n", i);
        BTreeNodeKeyValuePair* kvIterator = parent->startKeyPair;
        if(tree->keyComparator(spliterKvPair->key, kvIterator->key, tree->keySize) > 0) {
            while(kvIterator->next != NULL && tree->keyComparator(spliterKvPair->key, kvIterator->next->key, tree->keySize) >= 0) {
                kvIterator = kvIterator->next;
                i++;
            }
            //printf("next to %s\n", (char*)kvIterator->key);
            spliterKvPair->next = kvIterator->next;
            kvIterator->next = spliterKvPair;
            
        } else {
            spliterKvPair->next = parent->startKeyPair;
            parent->startKeyPair = spliterKvPair;
        }
        parent->keyCount++;
        //printf("index is --- %d\n", i);
        //btreePrintNode(parent);
        BTreeNode* nodeIterator = parent->startChild;
        i = i + 1;
        while(--i >= 0)
            nodeIterator = nodeIterator->next;
            //printf("rgregerg\n");
        rightNode->next = nodeIterator->next;
        nodeIterator->next = rightNode;
        parent->childCount++;
        //btreePrintNode(parent);
    }
    return parent;
}

int32_t btreeLexicalComparator(const void* key1, const void* key2, const size_t keySize) {
    return strncmp((char*)(key1), (char*)(key2), keySize);
}

void btreePrintNode(BTreeNode* node) {
    printf("==Printing Node==\n");

    printf("Keys: %d, Children: %d \n", node->keyCount, node->childCount);
    BTreeNodeKeyValuePair *kvIterator = node->startKeyPair;
    do {
        printf(" | Key: %d , Value: %s", *((int*)kvIterator->key) ,(char*)kvIterator->value);
    } while((kvIterator = kvIterator->next) != NULL);
    printf("\n==End Of Printing Node==\n");
    BTreeNode *nodeIterator = node->startChild;
    for(int i = 0; i < node->childCount; ++i) {
        btreePrintNode(nodeIterator);
        nodeIterator = nodeIterator->next;
    }
}