#ifndef BTREE_H
#define BTREE_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

typedef void*(*MemoryAllocatorFunctionType)(size_t);
typedef void*(*MemoryReallocatorFunctionType)(void*, size_t);
typedef void(*MemoryCollectorFunctionType)(void*);
typedef int32_t(*KeyComparatorFunctionType)(const void*, const void*, const size_t);

#define BTREE_SUCCESS 0
#define BTREE_ERROR 1

#define BTREE_IS_EMPTY(tree) (tree->root == NULL && tree->itemCount == 0 && tree->nodeCount == 0)
#define BTREE_NODE_IS_EMPTY(node) (node->keyCount == 0 && node->childCount == 0)
#define BTREE_NODE_IS_FULL(tree, node) (node->keyCount == tree->maxOrder)
#define BTREE_NODE_IS_LEAF(node) (node->childCount == 0)
#define BTREE_NODE_IS_ROOT(tree, node) (node == tree->root)
#define BTREE_NODE_HAS_MIN_ORDER(tree, node) (node->keyCount == tree->minOrder)

typedef struct BTreeNodeKeyValuePair {
    void *key;
    void *value;
    struct BTreeNodeKeyValuePair *next;
} BTreeNodeKeyValuePair;

typedef struct BTreeNode {
    BTreeNodeKeyValuePair *startKeyPair;
    size_t keyCount;
    struct BTreeNode *next;
    struct BTreeNode *startChild;
    size_t childCount;
} BTreeNode;

typedef struct {
    size_t keySize;
    size_t t;
    size_t maxOrder;
    size_t minOrder;
    size_t nodeCount;
    size_t itemCount;
    size_t splitIndex;
    MemoryAllocatorFunctionType memAllocator;
    MemoryReallocatorFunctionType memReallocator;
    MemoryCollectorFunctionType memCollector;
    MemoryCollectorFunctionType itemMemCollector;
    KeyComparatorFunctionType keyComparator;
    BTreeNode *root;
} BTree;

BTreeNode* btreeCreateEmptyNode(MemoryAllocatorFunctionType ma, size_t keyCount);
BTreeNodeKeyValuePair* btreeCreateKeyValuePair(MemoryAllocatorFunctionType ma, void* key, void* value, void* next);
void btreeInsertInEmptyNode(MemoryAllocatorFunctionType ma, BTreeNode* node, void* key, void* value);
BTreeNode* btreeSplitNode(BTree* tree, BTreeNode* parent, BTreeNode* node);
BTree* btreeInit(size_t t, size_t ks,
                    MemoryAllocatorFunctionType ma,
                    MemoryReallocatorFunctionType mra,
                    MemoryCollectorFunctionType mc,
                    MemoryCollectorFunctionType imc,
                    KeyComparatorFunctionType kc);
uint32_t btreeInsert(BTree* tree, void* key, void* value);
void*  btreeGet(BTree* tree, void* key);
uint32_t btreeRemove(BTree* tree, void* key);
int32_t btreeLexicalComparator(const void* key1, const void* key2, const size_t keySize);
void btreePrintNode(BTreeNode* node);
#endif
