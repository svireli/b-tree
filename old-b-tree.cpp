#include <iostream>
#include <string>
#include <forward_list>
#include <chrono>
#include <algorithm>

inline void log(std::string text) { std::cout << text << std::endl; }

template <typename T>
class BTreeNode {
  unsigned int childCount = 0;
  unsigned int keyCount = 0;
  unsigned int t = 0;
  std::forward_list<T> keys;
  std::forward_list<BTreeNode<T>*> children;

 public:
  BTreeNode(unsigned int _t) : t(_t) {}

  ~BTreeNode() {
	if(!this->isLeaf()) {
		auto it = this->children.begin();
		while(it != this->children.end()) {
			delete *it;
			it++;
		}
	}
  }

  inline bool isLeaf() { return this->childCount == 0; }

  inline bool isEmpty() { return this->keyCount == 0 && this->isLeaf(); }

  inline bool isFull() { return this->keyCount == 2 * t - 1; }

  void insertFirstKey(T key) {
    this->keys.push_front(key);
    this->keyCount = 1;
  }

  typename std::forward_list<BTreeNode<T>*>::iterator insertFirstChild(
      BTreeNode<T>* node) {
    this->children.push_front(node);
    this->childCount = 1;
    return this->children.begin();
  }

  void splitChild(
      unsigned int childIndex,
      typename std::forward_list<BTreeNode<T>*>::iterator childNodeIterator) {
    BTreeNode<T>* childNode = *childNodeIterator;
    BTreeNode<T>* newNode = new BTreeNode<T>(this->t);
    newNode->keyCount = this->t - 1;
    auto it = childNode->keys.begin();
    unsigned int i = this->t;
    while (--i != 1) ++it;
    newNode->keys.splice_after(newNode->keys.before_begin(), childNode->keys,
                               std::next(it), childNode->keys.end());
    if (!childNode->isLeaf()) {
      i = this->t;
      auto cit = childNode->children.begin();
      while (--i != 0) ++cit;
      newNode->children.splice_after(newNode->children.before_begin(),
                                     childNode->children, cit,
                                     childNode->children.end());
      childNode->childCount = this->t;
      newNode->childCount = this->t;
    }
    auto pcit = this->keys.before_begin();
    i = childIndex;
    while (i-- != 0) ++pcit;
    this->keys.splice_after(pcit, childNode->keys, it, childNode->keys.end());
    this->keyCount++;
    childNode->keyCount = this->t - 1;
    this->children.insert_after(childNodeIterator, newNode);
    this->childCount++;
  }

  void insertKey(T key) {
    if (this->isLeaf()) {
      if (key == this->keys.front()) return;
      if (key < this->keys.front()) {
        this->keys.push_front(key);
      } else {
        auto it = this->keys.begin();
        while (std::next(it) != this->keys.end() && key > *std::next(it)) ++it;
        auto nit = std::next(it);
        if (nit != this->keys.end() && key == *nit) return;
        this->keys.insert_after(it, key);
      }
      this->keyCount++;
    } else {
      auto it = this->keys.begin();
      unsigned int i = 0;
      while (it != this->keys.end() && key > *(it++)) i++;
      auto cit = this->children.begin();
      unsigned int childIndex = i;
      while (i-- != 0) cit++;
      auto childNode = (*cit);
      if (childNode->isFull()) {
        this->splitChild(childIndex, cit);
        if((key < (*std::next(cit))->keys.front()))
            childNode->insertKey(key);
        else
            (*(++cit))->insertKey(key);
      } else
        childNode->insertKey(key);
    }
  }

  T findKey(T key) {
	if(this->isLeaf()) {
		auto it = this->keys.begin();
		while(it != this->keys.end() && *it != key)
			++it;
		if(it == this->keys.end())
			return key;
		else
			return *it;
	} else {
		auto it = this->keys.begin();
		unsigned int i = 0;
		while (it != this->keys.end() && key > *(it)) {
            ++i;
            ++it;
		}
		if(it != this->keys.end() && key == *it)
            return *it;
		auto cit = this->children.begin();
		while (i-- != 0) cit++;
		return (*cit)->findKey(key);
	}
  }

  void print() {
    log("=== Printing Node ===");
    std::cout << "Node Key Count: " << this->keyCount
              << " Child Count: " << this->childCount << std::endl;

    for (auto key : this->keys) std::cout << " | " << key;
	std::cout << std::endl;

    for (BTreeNode<T>* node : this->children) node->print();
    log("=== Printing Node End ===");
  }
};

template <typename T>
class BTree {
  unsigned int t;
  unsigned int keyCount = 0;
  BTreeNode<T>* root;

 public:
  BTree(unsigned int _t) : t(_t) { root = nullptr; }

  ~BTree() {
    delete root;
  }

  void insert(T key) {
    if (this->root == nullptr) {
      this->root = new BTreeNode<T>(this->t);
      this->root->insertFirstKey(key);
    } else {
      if (this->root->isFull()) {
        BTreeNode<T>* nodeToSplit = this->root;
        this->root = new BTreeNode<T>(this->t);
        this->root->splitChild(0, this->root->insertFirstChild(nodeToSplit));
      }
      this->root->insertKey(key);
    }
  }

  T find(T key) {
	return this->root->findKey(key);
  }

  void print() {
    log("==== Printing Tree ====");
    std::cout << "Tree Key Count: " << this->keyCount << std::endl;
    if (this->root) this->root->print();
    log("==== Printing Tree End ====");
  }
};

class KeyValue {
	int key;
	int value;
public:
	KeyValue(int k, int v): key(k), value(v) { }
	~KeyValue() {  }
	int getKey() { return this->key; }
	int getValue() { return this->value; }
	inline bool operator > (KeyValue& rhs) {
		return this->key > rhs.getKey();
	}

	inline bool operator < (KeyValue& rhs) {
		return this->key < rhs.getKey();
	}

	inline bool operator != (KeyValue& rhs) {
		return this->key != rhs.getKey();
	}

	inline bool operator == (KeyValue& rhs) {
		return this->key == rhs.getKey();
	}

	friend std::ostream& operator<<(std::ostream& os, const KeyValue &kv) {
		os << kv.key;
		return os;
	}
};

#define TEN_MILLION 10000000
int* integers;

void generateAndFillIntegers()
{
	integers = new int[TEN_MILLION];
	for (size_t i = 0; i < TEN_MILLION; i++) {
		integers[i] = i - (TEN_MILLION / 2);
	}
	std::random_shuffle(integers, integers + TEN_MILLION);
}

int main() {
  generateAndFillIntegers();
  log("Random Shuffled List Insert!");
  for(int t = 3; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i <= TEN_MILLION; ++i) {
        tree.insert(integers[i]);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }

  log("Sorted List Insert!");
  for(int t = 3; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i <= TEN_MILLION; ++i) {
        tree.insert(i);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }

  log("Reverse sorted list insert!");
  for(int t = 3; t <= 6; ++t) {
    std::cout << " T = " << t << std::endl;
    BTree<int> tree(t);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, finish;
    start = std::chrono::high_resolution_clock::now();
    for (int i = TEN_MILLION; i >0; --i) {
        tree.insert(i);
    }
    finish = std::chrono::high_resolution_clock::now();
    std::cout << "Fill BTree "
    << TEN_MILLION << ", Milliseconds: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count()
    << std::endl;
  }
  return 0;
}
